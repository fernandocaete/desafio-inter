# Desafio Inter

Desafio para o Banco Inter

# Detalhes do projeto

O Projeto inicialmente foi estruturado em duas pastas (crudUsuario e digitoUnico), com a intenção de construir um serviço no digitoUnico e re-utilizar no crudUsuario, mas devido
a falta de tempo, tive que juntar tudo no mesmo projeto, sendo assim todo o desafio está implementado no crudUsuario.

Sobre a chave publica para criptografar o login e email, quando não informada o sistema gera um par de chaves (publica e privada) e retorna com os dados criptografados.

# Rodando a aplicação

Para rodar a aplicação basta utilizar o seguinte comando:

```java
mvn spring-boot:run
```

Para rodar os testes unitários, use o seguinte comando:
```java
mvn test
```

# APIs de acesso

O Acesso ao Swagger está na URL: http://localhost:8080/swagger-ui.html

API´s para acessar o crud de usuário:

#### H4 Criar
URL: http://localhost:8080/api/v1/usuarios

Metodo: POST

Exemplo:

```java

{
  "email": "string",
  "nome": "string",
  "publicKey": "string"
}

```

#### H4 Atualizar
URL: http://localhost:8080/api/v1/digitoUnico/{idUsuario}

Metodo: PATCH

Parâmetros: idUsuario
```java

{
  "email": "string",
  "nome": "string",
  "publicKey": "string"
}

```

#### H4 Listar
URL: http://localhost:8080/api/v1/usuarios

Metodo: GET

#### H4 Deletar
URL: http://localhost:8080/api/v1/usuarios/{idUsuario}

Metodo: DELETE

API do dígito unico:

#### H4 Calcular
URL: http://localhost:8080/api/v1/digitoUnico

Metodo: PUT

```java

{
  "idUsuario": 0,
  "multiplicador": 0,
  "numero": "string"
}

```

#### H4 Obter lista de números gerados
URL: http://localhost:8080/api/v1/digitoUnico/{idUsuario}

Metodo: GET

Parâmetros: idUsuario
```java

{
  "idUsuario": 0,
  "multiplicador": 0,
  "numero": "string"
}

```

# Referências técnicas de estudo/implementação

- https://hellokoding.com/restful-apis-example-with-spring-boot-integration-test-with-mockmvc-ui-integration-with-vuejs/
- https://www.javacodegeeks.com/2017/12/choosing-java-cryptographic-algorithms-part-3-public-private-key-asymmetric-encryption.html
- https://start.spring.io/
- https://www.mkyong.com/java/java-asymmetric-cryptography-example/
- https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api

# Ferramenta para teste das chaves RSA
- https://www.devglan.com/online-tools/rsa-encryption-decryption