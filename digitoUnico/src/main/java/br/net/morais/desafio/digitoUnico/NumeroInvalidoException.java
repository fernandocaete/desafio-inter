package br.net.morais.desafio.digitoUnico;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NumeroInvalidoException extends Exception {

	private static final long serialVersionUID = 1L;

	public NumeroInvalidoException(String message) {
		super(message);
	}
}
