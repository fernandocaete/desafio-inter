package br.net.morais.desafio.digitoUnico;

import java.util.LinkedHashMap;

public class CacheMap extends LinkedHashMap<String, Integer> {
	
	private static final long serialVersionUID = 1L;

	@Override
	protected boolean removeEldestEntry(java.util.Map.Entry<String, Integer> arg0) {
		return size() > 10;
	}
}