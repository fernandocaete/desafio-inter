package br.net.morais.desafio.digitoUnico.service;

import java.util.Map;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import br.net.morais.desafio.digitoUnico.CacheMap;
import br.net.morais.desafio.digitoUnico.NumeroInvalidoException;
import br.net.morais.desafio.digitoUnico.model.DigitoUnicoDTO;

@Service
public class NumeroUnicoService {
	private static final Map<String, Integer> CACHE = new CacheMap();

	public Integer digitoUnico(DigitoUnicoDTO digitoUnicoDTO)
			throws NumeroInvalidoException {
		if (!StringUtils.isNumeric(digitoUnicoDTO.getNumero())) {
			throw new NumeroInvalidoException("Numero inávildo: " + digitoUnicoDTO.getNumero());
		}
		
		if (CACHE.containsKey(digitoUnicoDTO.getNumero())) {
			return CACHE.get(digitoUnicoDTO.getNumero());
		}

		if (digitoUnicoDTO.getNumero().length() == 1) {
			CACHE.put(digitoUnicoDTO.getNumero(), Integer.valueOf(digitoUnicoDTO.getNumero()));
			return Integer.valueOf(digitoUnicoDTO.getNumero());
		}

		Integer total = Stream.of(digitoUnicoDTO.getNumero().split("")).mapToInt(e -> new Integer(e)).sum();

		if (digitoUnicoDTO.getMultiplicador() != null && digitoUnicoDTO.getMultiplicador() > 0) {
			total = total * digitoUnicoDTO.getMultiplicador();
		}
		
		CACHE.put(digitoUnicoDTO.getNumero(), total);

		return total;
	}
}
