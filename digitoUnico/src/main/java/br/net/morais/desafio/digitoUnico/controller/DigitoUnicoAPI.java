package br.net.morais.desafio.digitoUnico.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.morais.desafio.digitoUnico.NumeroInvalidoException;
import br.net.morais.desafio.digitoUnico.model.DigitoUnicoDTO;
import br.net.morais.desafio.digitoUnico.service.NumeroUnicoService;

@RestController
@RequestMapping("/api/v1")
public class DigitoUnicoAPI {
	
	@Autowired
	private NumeroUnicoService numeroUnicoService;

	@PutMapping("/digitoUnico")
	public ResponseEntity<Integer> digitoUnico(@RequestBody DigitoUnicoDTO digitoUnicoDTO)
			throws NumeroInvalidoException {
		return ResponseEntity.ok(numeroUnicoService.digitoUnico(digitoUnicoDTO));
	}

}
