package br.net.morais.desafio.digitoUnico;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import br.net.morais.desafio.digitoUnico.model.DigitoUnicoDTO;
import br.net.morais.desafio.digitoUnico.service.NumeroUnicoService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {DigitoUnicoApplication.class, Config.class})
public class DigitoUnicoTest {

	@Autowired
	private NumeroUnicoService numeroUnicoService;


	@Test
	public void umDigitoTest() throws Exception {
		Assert.assertTrue(new Integer(1).equals(numeroUnicoService.digitoUnico(new DigitoUnicoDTO("1", null))));
	}

}
