package br.net.morais.desafio.digitoUnico;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("br.net.morais.desafio.digitoUnico")
public class Config {

}
