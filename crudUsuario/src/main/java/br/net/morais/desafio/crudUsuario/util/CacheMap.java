package br.net.morais.desafio.crudUsuario.util;

import java.util.LinkedHashMap;

/**
 * Uma especialização do Map do pacote java.util para conter apenas 10 itens e ir sobrescrevendo os primeiros informados.
 * 
 * @author fernando.morais
 *
 */
public class CacheMap extends LinkedHashMap<String, Integer> {
	
	private static final long serialVersionUID = 1L;

	@Override
	protected boolean removeEldestEntry(java.util.Map.Entry<String, Integer> arg0) {
		return size() > 10;
	}
}