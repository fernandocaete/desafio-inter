package br.net.morais.desafio.crudUsuario.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Quando o número informado na API contém letras e caracteres, essa excption deve ser levantada.
 * 
 * @author fernando.morais
 *
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NumeroInvalidoException extends Exception {

	private static final long serialVersionUID = 1L;

	public NumeroInvalidoException(String message) {
		super(message);
	}
}
