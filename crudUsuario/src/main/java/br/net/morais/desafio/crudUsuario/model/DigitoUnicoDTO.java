package br.net.morais.desafio.crudUsuario.model;

public class DigitoUnicoDTO {

	private String numero;
	private Integer multiplicador;
	private Integer idUsuario;

	public DigitoUnicoDTO() {
	}

	public DigitoUnicoDTO(String numero, Integer multiplicador) {
		super();
		this.numero = numero;
		this.multiplicador = multiplicador;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Integer getMultiplicador() {
		return multiplicador;
	}

	public void setMultiplicador(Integer multiplicador) {
		this.multiplicador = multiplicador;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
}
