package br.net.morais.desafio.crudUsuario.util;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import org.springframework.stereotype.Component;

/**
 * Utilitário para manipular as chaves pública/privada
 * 
 * @author fernando.morais
 *
 */
@Component
public class RSAUtil {

	protected Cipher cipher;

	@PostConstruct
	public void init() throws NoSuchAlgorithmException, NoSuchPaddingException {
		cipher = Cipher.getInstance("RSA");

	}

	/**
	 * Desencriptografa os dados.
	 * 
	 * @param message
	 * @param key
	 * @return
	 */
	public String decrypt(byte[] message, PrivateKey key) {
		try {
			cipher.init(Cipher.DECRYPT_MODE, key);
			return new String(cipher.doFinal(message), "UTF-8");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Encriptografam os dados
	 * 
	 * @param message
	 * @param key
	 * @return
	 */
	public String encrypt(String message, PublicKey key) {
		try {
			cipher.init(Cipher.ENCRYPT_MODE, key);
			return new String(Base64.getEncoder().encode(cipher.doFinal(message.getBytes("UTF-8"))));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Gera um par de chaves randomico.
	 * 
	 * @return
	 */
	public KeyPair generateKey() {
		KeyPairGenerator keyGen;
		try {
			keyGen = KeyPairGenerator.getInstance("RSA");
			keyGen.initialize(2048);
			return keyGen.generateKeyPair();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Recupera a chave privada com base no array de bytes.
	 * 
	 * @param encodedByteArrayForPrivateKey
	 * @return
	 */
	public PrivateKey recoveryPrivateKey(byte[] encodedByteArrayForPrivateKey) {
		try {
			PrivateKey privateKey = KeyFactory.getInstance("RSA")
					.generatePrivate(new PKCS8EncodedKeySpec(encodedByteArrayForPrivateKey));

			return privateKey;
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Recupera a chave pública com base no array de bytes.
	 * 
	 * @param encodedByteArrayForPublicKey
	 * @return
	 */
	public PublicKey recoveryPublicKey(byte[] encodedByteArrayForPublicKey) {
		try {
			PublicKey publicKey = KeyFactory.getInstance("RSA")
					.generatePublic(new X509EncodedKeySpec(encodedByteArrayForPublicKey));

			return publicKey;
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
}
