package br.net.morais.desafio.crudUsuario.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.net.morais.desafio.crudUsuario.model.NumeroUnicoUsuario;
import br.net.morais.desafio.crudUsuario.model.Usuario;

public interface NumeroUnicoRepository extends JpaRepository<NumeroUnicoUsuario, Integer> {

	public List<NumeroUnicoUsuario> findByUsuario(Usuario usuario);
}
