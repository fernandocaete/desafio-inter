package br.net.morais.desafio.crudUsuario.service;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.net.morais.desafio.crudUsuario.model.Usuario;
import br.net.morais.desafio.crudUsuario.repo.UsuarioRepository;
import br.net.morais.desafio.crudUsuario.util.RSAUtil;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private RSAUtil rsaUtil;

	public List<Usuario> findAll() {
		return usuarioRepository.findAll();
	}

	public Optional<Usuario> findById(Integer id) {
		return usuarioRepository.findById(id);
	}

	public Usuario save(Usuario usuario) {
		PublicKey publicKey = null;
		PrivateKey privateKey = null;
		if (usuario.getPublicKey() != null) {
			byte[] pkArray = Base64.getDecoder().decode(usuario.getPublicKey());
			publicKey = rsaUtil.recoveryPublicKey(pkArray);
		} else {
			KeyPair kp = rsaUtil.generateKey();
			publicKey = kp.getPublic();
			privateKey = kp.getPrivate();
			
			usuario.setPrivateKey(Base64.getEncoder().encodeToString(privateKey.getEncoded()));
			usuario.setPublicKey(Base64.getEncoder().encodeToString(publicKey.getEncoded()));
		}
		usuario.setNome(rsaUtil.encrypt(usuario.getNome(), publicKey));
		usuario.setEmail(rsaUtil.encrypt(usuario.getEmail(), publicKey));
		
		
		
		return usuarioRepository.save(usuario);
	}
	
	public Usuario update(Usuario updatingUsuario, Integer idUsuario) {
		Optional<Usuario> stockOptional = findById(idUsuario);
		if (!stockOptional.isPresent()) {
			return null;
		}
		
		Usuario usuario = stockOptional.get();
		
		byte[] pkArray = Base64.getDecoder().decode(usuario.getPublicKey());
		PublicKey pk = rsaUtil.recoveryPublicKey(pkArray);

		if (!StringUtils.isEmpty(updatingUsuario.getNome()) && !updatingUsuario.getNome().equals(usuario.getNome()))
			usuario.setNome(rsaUtil.encrypt(updatingUsuario.getNome(), pk));
		if (!Objects.isNull(updatingUsuario.getEmail()) && !updatingUsuario.getNome().equals(usuario.getNome()))
			usuario.setEmail(rsaUtil.encrypt(updatingUsuario.getNome(), pk));
		
		return usuarioRepository.save(usuario);
	}

	public void deleteById(Integer id) {
		usuarioRepository.deleteById(id);
	}
}
