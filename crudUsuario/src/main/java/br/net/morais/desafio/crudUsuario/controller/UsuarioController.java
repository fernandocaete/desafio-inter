package br.net.morais.desafio.crudUsuario.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.morais.desafio.crudUsuario.model.Usuario;
import br.net.morais.desafio.crudUsuario.service.UsuarioService;

@RestController
@RequestMapping("/api/v1/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@GetMapping
	public ResponseEntity<List<Usuario>> findAll() {
		return ResponseEntity.ok(usuarioService.findAll());
	}

	@PostMapping
	public ResponseEntity create(@RequestBody Usuario usuario) {
		return ResponseEntity.status(HttpStatus.CREATED).body(usuarioService.save(usuario));
	}

	@GetMapping("/{idUsuario}")
	public ResponseEntity<Usuario> findById(@PathVariable Integer idUsuario) {
		Optional<Usuario> stockOptional = usuarioService.findById(idUsuario);
		if (!stockOptional.isPresent()) {
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(stockOptional.get());
	}

	@PatchMapping("/{idUsuario}")
	public ResponseEntity<Usuario> update(@PathVariable Integer idUsuario, @RequestBody Usuario updatingUsuario) {
		Usuario savedUser = usuarioService.update(updatingUsuario, idUsuario);
		if (savedUser == null) {
			return ResponseEntity.badRequest().build();
		}

		return ResponseEntity.accepted().body(savedUser);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable Integer id) {
		usuarioService.deleteById(id);

		return ResponseEntity.accepted().build();
	}
}
