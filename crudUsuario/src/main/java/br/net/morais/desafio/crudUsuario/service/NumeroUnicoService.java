package br.net.morais.desafio.crudUsuario.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.net.morais.desafio.crudUsuario.model.DigitoUnicoDTO;
import br.net.morais.desafio.crudUsuario.model.NumeroUnicoUsuario;
import br.net.morais.desafio.crudUsuario.model.Usuario;
import br.net.morais.desafio.crudUsuario.repo.NumeroUnicoRepository;
import br.net.morais.desafio.crudUsuario.repo.UsuarioRepository;
import br.net.morais.desafio.crudUsuario.util.CacheMap;
import br.net.morais.desafio.crudUsuario.util.NumeroInvalidoException;

@Service
public class NumeroUnicoService {
	private static final Map<String, Integer> CACHE = new CacheMap();
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private NumeroUnicoRepository numeroUnicoRepository;

	/**
	 * Calcula o dígito único conforme as regras do desafio.
	 * 
	 * @param digitoUnicoDTO
	 * @return
	 * @throws NumeroInvalidoException
	 */
	public Integer digitoUnico(DigitoUnicoDTO digitoUnicoDTO)
			throws NumeroInvalidoException {
		if (!StringUtils.isNumeric(digitoUnicoDTO.getNumero())) {
			throw new NumeroInvalidoException("Numero inávildo: " + digitoUnicoDTO.getNumero());
		}
		
		Integer total = null;
		
		if (CACHE.containsKey(digitoUnicoDTO.getNumero())) {
			total = CACHE.get(digitoUnicoDTO.getNumero());
		}

		if (digitoUnicoDTO.getNumero().length() == 1) {
			CACHE.put(digitoUnicoDTO.getNumero(), Integer.valueOf(digitoUnicoDTO.getNumero()));
			total = Integer.valueOf(digitoUnicoDTO.getNumero());
		}
		
		if (total != null) {
			persistNumeroUnico(digitoUnicoDTO, Integer.valueOf(digitoUnicoDTO.getNumero()));
			return total;
		}

		total = Stream.of(digitoUnicoDTO.getNumero().split("")).mapToInt(e -> new Integer(e)).sum();

		if (digitoUnicoDTO.getMultiplicador() != null && digitoUnicoDTO.getMultiplicador() > 0) {
			total = total * digitoUnicoDTO.getMultiplicador();
		}
		
		CACHE.put(digitoUnicoDTO.getNumero(), total);
		
		persistNumeroUnico(digitoUnicoDTO, total);

		return total;
	}

	/**
	 * Recupra uma lista de dígitos unicos informados para um determinado usuário.
	 * 
	 * @param digitoUnicoDTO
	 * @param total
	 */
	private void persistNumeroUnico(DigitoUnicoDTO digitoUnicoDTO, Integer total) {
		if (digitoUnicoDTO.getIdUsuario() != null) {
			Optional<Usuario> usuario = usuarioRepository.findById(digitoUnicoDTO.getIdUsuario());
			if (usuario.isPresent()) {
				NumeroUnicoUsuario nuUsuario = new NumeroUnicoUsuario();
				nuUsuario.setResultado(total);
				nuUsuario.setUsuario(usuario.get());
				nuUsuario.setNumeroEntrada(Integer.valueOf(digitoUnicoDTO.getNumero()));
				nuUsuario.setMultiplicador(digitoUnicoDTO.getMultiplicador());
				numeroUnicoRepository.save(nuUsuario);
			}
		}
	}
	
	public List<NumeroUnicoUsuario> findByUsuario(Integer idUsuario) {
		Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);
		if (usuario.isPresent()) {
			return numeroUnicoRepository.findByUsuario(usuario.get());
		}
		return null;
	}
}
