package br.net.morais.desafio.crudUsuario.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.net.morais.desafio.crudUsuario.model.DigitoUnicoDTO;
import br.net.morais.desafio.crudUsuario.model.NumeroUnicoUsuario;
import br.net.morais.desafio.crudUsuario.service.NumeroUnicoService;
import br.net.morais.desafio.crudUsuario.util.NumeroInvalidoException;

@RestController
@RequestMapping("/api/v1/digitoUnico")
public class DigitoUnicoAPI {
	
	@Autowired
	private NumeroUnicoService numeroUnicoService;

	@PutMapping
	public ResponseEntity<Integer> digitoUnico(@RequestBody DigitoUnicoDTO digitoUnicoDTO)
			throws NumeroInvalidoException {
		return ResponseEntity.ok(numeroUnicoService.digitoUnico(digitoUnicoDTO));
	}
	
	@GetMapping("/{idUsuario}")
	public List<NumeroUnicoUsuario> obterPorUsuario(Integer idUsuario) {
		return numeroUnicoService.findByUsuario(idUsuario);
	}

}
