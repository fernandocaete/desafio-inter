package br.net.morais.desafio.crudUsuario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import br.net.morais.desafio.crudUsuario.service.UsuarioService;
import br.net.morais.desafio.crudUsuario.util.RSAUtil;

@ComponentScan(basePackageClasses = {
        UsuarioService.class,
        CrudUsuarioApplication.class,
        RSAUtil.class
})
@SpringBootApplication
public class TestConfiguration {
    public static void main(String[] args) {
        SpringApplication.run(TestConfiguration.class, args);
    }
}
