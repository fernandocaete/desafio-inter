package br.net.morais.desafio.crudUsuario;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;

import javax.annotation.PostConstruct;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import br.net.morais.desafio.crudUsuario.model.Usuario;
import br.net.morais.desafio.crudUsuario.service.UsuarioService;
import br.net.morais.desafio.crudUsuario.util.RSAUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestConfiguration.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CrudUsuarioApplicationTests {
	
	private String publicKeyString = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiPWxCyTsHqz+UeQ0Z7ExYhCp+G82myKDCoPBTTP2/9wfXiSmfA+pil7rjDqP/Mk2eHiCcXtIbMps/h/Z1MY9DhimYMhrtxkeowBrniecEO5sVtQB5kuWOJWdGInTVdGYZoMptbJ+FzfNQZSsgmUOo/veeqTwpKqkWIrCMtUy12apZM5He1VSOJcqRB/sHWm98mwqpNQrGQskkbLtMkxrx0hhx8UWXXUaXBjOsS69DH7Srt3novYkOOsxc3IbF0WGR/j8LsYF+rNOXytBpG7fGf5k7E3wOmxUV4VIOLlR8KV3TR8+bZWBFIAwHhVDQOZgWWKQHcdij2fmcTZeeYdtqwIDAQAB";
	private String privateKeyString = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCI9bELJOwerP5R5DRnsTFiEKn4bzabIoMKg8FNM/b/3B9eJKZ8D6mKXuuMOo/8yTZ4eIJxe0hsymz+H9nUxj0OGKZgyGu3GR6jAGueJ5wQ7mxW1AHmS5Y4lZ0YidNV0Zhmgym1sn4XN81BlKyCZQ6j+956pPCkqqRYisIy1TLXZqlkzkd7VVI4lypEH+wdab3ybCqk1CsZCySRsu0yTGvHSGHHxRZddRpcGM6xLr0MftKu3eei9iQ46zFzchsXRYZH+PwuxgX6s05fK0Gkbt8Z/mTsTfA6bFRXhUg4uVHwpXdNHz5tlYEUgDAeFUNA5mBZYpAdx2KPZ+ZxNl55h22rAgMBAAECggEAICwu6aJDwTT8DPqhgNYz9ohZlR1y42BpDfhDWUJivFnWGIRdg3keZ33CsU80cbpGjGgqpEPEkKHAlrWeswfaYcxSkRujigBhnQLFRQKi+6s2UfM9ev64jBRBMfl2Zot01GftTVxfmE6obcZh+8OHe+f3rDHAwb3AG3iFA/HnHogMHFtMhiYIgBz/iVWdltZwRab9t7V0hnZG67tqcd5qrBpzYJLx/2dguWKM4uHYeW0OTUFGnj/YIFiJOndMN3EkP2Ehx+8aPrBD1ptVF7YMzoAJHw2zZMjwlzTBb2nRr0Jx7lXezJKyNjbGW2ei75EzSToC5T4eEUdGaxe6iOJxuQKBgQDFaPn6VoVwQ6Mz2xoT0ZDlQAEuCXSWU3pukzK86ZI4dhiHIYkXDJeT1pp3sajWxAYjEO7Co/gPzaW1YSfWkkkGJgKJ5nDUJ3/wfNO6+/zvGRvfJvv+zWBsKeyRf9IDMOxQkhQuXSPsAqSSvP8DtC8h38dLdphGo524IxfUTTFZxwKBgQCxm8IPwnGNrgXgp36e+CdqZZJsjoy5cIds+h8sY2K1HNqoKfqR+dL2zYqbfCB9yImV2pHXsmBENPvtEF56Fcij+cO+G9HKg46VXCDShIqKHBvZJV7weFT3sLmtjpiAkUKihNYymEaiPcIQP3JuuNzpIQlmBdW1Jg0iJLYRPRCs/QKBgQC73UsHkY8rZzLJBpmqgqUBhgeZaCYraQX6dNq0LROZ58kv0BpGdRVK+8/EWK30tFeix1pwoNpRL8Oi+PKQoGYeE84WrdZyD6URl7lnGKd8YsjDdLYbphc82Cjpzlpt7wD1LA9IkBq/X0R+4J1PSQoRkoR2rEX983cE5WCoCU4NwQKBgCChtnkAx645P/kzrO72Nv+0Y7+wPSjBBRC/r8SPCg6um3n0JIZ3eQxBXWsN1kZl9ulifBM+QbOmbHb03/bcU73U4OLn55amrybAA4PKOE4BthgiZpwV776GaMb7gQd2KOEFOPmYwrmCUUp87cBsFKJ8oGZvQgGDWTxhtFXce1jdAoGAEaczh6Ic91iu59QNLnsK7CjA8+11hXX2cj9hPZvHBdNE3Jpxbii654nIBcWhU9PV4pHrKAetlvmFp3dUwET+NX28AQ3GfuuTkIHEN9TTMXunGpzbj6oyQUe+r73QMz1ylhygbnOfU8AYGAZYKdXYZ5z8XsLwKrG28uSA/wwg/hw=";
	
	private PublicKey publicKey;
	private PrivateKey privateKey;
	
	@PostConstruct
	public void init() {
		privateKey = rsaUtil.recoveryPrivateKey(Base64.decode(privateKeyString));
		publicKey = rsaUtil.recoveryPublicKey(Base64.decode(publicKeyString));
	}
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private RSAUtil rsaUtil;

	@Test
	public void test1NovoUsuarioComPublicKey() {
		Usuario usuario = new Usuario();
		usuario.setNome("Fernando");
		usuario.setEmail("fernandocaete@gmail.com");
		usuario.setPublicKey(publicKeyString);
		
//		Usuario updateUsuario = new Usuario();
//		usuario.setNome("Fernando Morais");
		Usuario savedUser = usuarioService.save(usuario);
		
		String nome = rsaUtil.decrypt(Base64.decode(savedUser.getNome()), privateKey);
		Assert.assertEquals(nome, "Fernando");
//		usuarioService.deleteById(1);
	}
	
	@Test
	public void test2NovoUsuarioSemPublicKey() {
		Usuario usuario = new Usuario();
		usuario.setNome("Fernando");
		usuario.setEmail("fernandocaete@gmail.com");
		
		Usuario savedUser = usuarioService.save(usuario);
		
		String nome = rsaUtil.decrypt(Base64.decode(savedUser.getNome()), rsaUtil.recoveryPrivateKey(Base64.decode(usuario.getPrivateKey())));
		Assert.assertEquals(nome, "Fernando");
	}

	@Test
	public void test3AtualizarUsuario() {
		Usuario usuario = new Usuario();
		usuario.setNome("Fernando Morais");
		usuario.setEmail("fernando@morais.net.br");
		usuario.setPublicKey(publicKeyString);
		
		Usuario savedUser = usuarioService.update(usuario, 1);
		String nome = rsaUtil.decrypt(Base64.decode(savedUser.getNome()), privateKey);
		Assert.assertEquals(nome, "Fernando Morais");
		
		String encodedNome = savedUser.getNome();
		savedUser = usuarioService.update(new Usuario(publicKeyString), 1);
		Assert.assertEquals(encodedNome, savedUser.getNome());
		
		savedUser = usuarioService.update(new Usuario(), 5);
		Assert.assertNull(savedUser);
	}
	
	@Test
	public void test4PesquisarUsuarios() {
		List<Usuario> usuarios = usuarioService.findAll();
		Usuario usuario1 = usuarios.stream().filter(e -> e.getId().equals(1)).findFirst().get();
		
		String nome = rsaUtil.decrypt(Base64.decode(usuario1.getNome()), privateKey);
		Assert.assertEquals(nome, "Fernando Morais");
	}
	
	@Test
	public void test5DeleteUsuario() {
		usuarioService.deleteById(2);
	}
}
